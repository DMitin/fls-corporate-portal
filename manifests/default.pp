Exec { path => [ "/bin/", "/sbin/" , "/usr/bin/", "/usr/sbin/" ] }
package { 'mc':
  ensure   => present,
}

package {'git-core':
    ensure => latest
}


#maven
class { "maven::maven":
  version => "3.2.5", # version to install

}

#nginx
class { 'nginx': 
  sendfile => 'off'
}

exec{'retrieve_vertx':
  command => "mkdir /vagrant/vertx;
  			  wget https://bintray.com/artifact/download/vertx/downloads/vert.x-3.0.0-full.tar.gz -O /vagrant/vertx/vert.x-3.0.0-full.tar.gz ",
  path => "/usr/local/bin:/bin:/usr/bin",
  creates => "/vagrant/vertx/vert.x-3.0.0-full.tar.gz",
  logoutput => on_failure
}

exec{'tar_vertx':
  command => "tar -xzf /vagrant/vertx/vert.x-3.0.0-full.tar.gz -C /vagrant/vertx/ ",
  path => "/usr/local/bin:/bin:/usr/bin",
  creates => "/vagrant/vertx/vert.x-3.0.0",
  require =>  Exec["retrieve_vertx"]
}

file{'/vagrant/vertx/vert.x-3.0.0':
#  require => [Exec["retrieve_vertx"], Exec["tar_vertx"]]
	require =>  Exec["tar_vertx"]
}

# neo4j
exec{'retrieve_neo4j':
  command => "mkdir /home/vagrant/neo4j;
  			  wget https://www.dropbox.com/s/u8h7uvimj7cugcb/neo4j-community-2.2.5-unix.tar.gz -O /home/vagrant/neo4j/neo4j-community-2.2.5-unix.tar.gz ",
  path => "/usr/local/bin:/bin:/usr/bin",
  creates => "/home/vagrant/neo4j/neo4j-community-2.2.5-unix.tar.gz",
  logoutput => on_failure
}

exec{'tar_neo4j':
  command => "tar -xzf /home/vagrant/neo4j/neo4j-community-2.2.5-unix.tar.gz -C /home/vagrant/neo4j ",
  path => "/usr/local/bin:/bin:/usr/bin",
  creates => "/home/vagrant/neo4j-2.2.5",
  require =>  Exec["retrieve_neo4j"]
}

file{'/home/vagrant/neo4j/neo4j-community-2.2.5':
	require =>  Exec["tar_neo4j"]
}

#elasticsearch

exec{'retrieve_es':
  command => "mkdir /home/vagrant/es;
  			  wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.2.tar.gz -O /home/vagrant/es/elasticsearch-1.7.2.tar.gz ",
  path => "/usr/local/bin:/bin:/usr/bin",
  creates => "/home/vagrant/es/elasticsearch-1.7.2.tar.gz",
  logoutput => on_failure
}

exec{'tar_es':
  command => "tar -xzf /home/vagrant/es/elasticsearch-1.7.2.tar.gz -C /home/vagrant/es/ ",
  path => "/usr/local/bin:/bin:/usr/bin",
  creates => "/home/vagrant/es/elasticsearch-1.7.2",
  require =>  Exec["retrieve_es"]
}

file{'/home/vagrant/es/neo4j-2.2.5':
	require =>  Exec["tar_es"]
}