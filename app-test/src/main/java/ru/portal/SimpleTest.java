package ru.portal;

import com.jayway.restassured.RestAssured;

import    com.jayway.restassured.matcher.RestAssuredMatchers.*;
import com.jayway.restassured.response.Response;
import    org.hamcrest.Matchers.*;

import static com.jayway.restassured.RestAssured.*;

/**
 * Created by DMitin on 05.11.2015.
 */
public class SimpleTest {
    public static void main(String[] args) {
        //get();

        RestAssured.port = 8080;
        /*
        Response response = post("/loginhandler");
        String cookieValue = response.getCookie("vertx-web.session");
        */
        post("/loginhandler").then().using().cookie("vertx-web.session");

        //System.out.println(cookieValue);
    }
}
