package skeleton;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.response.ResponseBody;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.HashMap;
import java.util.Map;

import static com.jayway.restassured.RestAssured.given;


public class Stepdefs {
    @Given("^I have (\\d+) cukes in my belly$")
    public void I_have_cukes_in_my_belly(int cukes) throws Throwable {


        RestAssured.port = 8081;

        Map<String, Object> jsonAsMap = new HashMap<>();
        jsonAsMap.put("firstName", "John");
        jsonAsMap.put("lastName", "Doe");
        jsonAsMap.put("middleName", "Doe");

        Response response =   given().
                contentType(ContentType.JSON).
                body(jsonAsMap).
                post("/employee");
        ResponseBody body = response.body();
        JsonPath jp = body.jsonPath();
        String id = jp.getString("id");
        System.out.println(id);
    }

    @When("^I wait (\\d+) hour$")
    public void wait(int cukes) throws Throwable {

    }


    @Then("^my belly should growl$")
    public void growl() throws Throwable {

    }
}
