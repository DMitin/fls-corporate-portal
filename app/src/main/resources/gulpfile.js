var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var proxy = require('proxy-middleware');
var url = require('url');
var request = require('request');




var ENV = {
    TEST: "newportal.domain.corp:8082",
    LOCAL: "localhost:8082"
};




gulp.task('browser-sync', function() {

    var proxyOptions = url.parse('http://'+ ENV.TEST +'/rest/');
    proxyOptions.route = '/rest';

    browserSync.init({

        server: {
            baseDir: "webroot/",
            index: "index.html",
            middleware: [proxy(proxyOptions)]
        },

        /*
        proxy: {
            target: "localhost:8082",
            middleware: function (req, res, next) {
                console.log(req.url);
                next();
            }
        }
        */
    });
});


gulp.task('watch', function() {
    gulp.watch(['webroot/**/*.*'], function() {
        browserSync.reload();
    });
});

gulp.task('default', ['watch', 'browser-sync']);
