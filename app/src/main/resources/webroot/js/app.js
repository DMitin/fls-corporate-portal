// For any third party dependencies, like jQuery, place them in the js/vendor folder.

// Configure loading modules from the lib directory,
// except for 'app' ones, which are in a sibling
// directory.
requirejs.config({
    baseUrl: 'js/app',
    shim: {
        "webix": {
            exports: "webix"
        }
    },
    paths: {
        app: '.',

        //webix: "../vendor/webix/webix_debug",
        backbone: '../vendor/backbone/backbone-min',
        jquery: '../vendor/jquery/jquery.min',
        underscore: '../vendor/underscore/underscore',
        bootstrap: '../vendor/bootstrap/bootstrap.min',
        handlebars: '../vendor/handlebars/handlebars.amd.min',

        marionette: '../vendor/marionette/backbone.marionette',
        "backbone.wreqr": "../vendor/backbone.wreqr/backbone.wreqr",
        "backbone.babysitter": "../vendor/backbone.babysitter/backbone.babysitter",
        "backbone.paginator": "../vendor/backbone.paginator/backbone.paginator",
        "backbone.select": "../vendor/backbone.select/backbone.select",
        text: '../vendor/text/text'
    }
});

// Start loading the main app file. Put all of
// your application logic in there.
requirejs(['app/main', "marionette", "handlebars"],
    function(main, marionette, Handlebars) {

    /*
        Тюнинг Marionette
     */

    // Чтоб marionette из коробки работала с handlebars
        Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate, options) {
            // use Handlebars.js to compile the template
            return Handlebars.compile(rawTemplate);
        };

        Marionette.TemplateCache.prototype.loadTemplate = function(templateId, options) {
            if (!templateId) {
                throw new Marionette.Error({
                    name: 'NoTemplateError',
                    message: 'Could not find template: "' + templateId + '"'
                });
            }
            return templateId;
        }
});
/*
Marionette.TemplateCache.prototype.compileTemplate = function(rawTemplate, options) {
    // use Handlebars.js to compile the template
    return Handlebars.compile(rawTemplate);
}
*/

requirejs.onError = function (err) {
    console.log(err.requireType);
    console.log('modules: ' + err.requireModules);
    throw err;
};
