/**
 * Created by DMitin on 05.12.2015.
 */
define(['backbone'], function(Backbone) {


    /*
        ������ ���������� - � ������� ������� (������� ������������ � �������)
     */
    var Employee = Backbone.Model.extend({
        defaults: {
            imageSrc: "images/noImage.jpeg"
        },

        setBasicAttributes: function(searchModel) {
            this.set('name', searchModel.get('name'));
            this.set('secondName', searchModel.get('secondName'));
        }
    });

    return Employee;


});