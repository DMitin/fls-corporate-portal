/**
 * Created by DMitin on 02.12.2015.
 */
define(["backbone"], function(Backbone) {
    var SearchUser = Backbone.Model.extend({});

    return SearchUser;
});