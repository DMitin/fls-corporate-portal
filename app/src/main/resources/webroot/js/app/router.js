/**
 * Created by DMitin on 24.11.2015.
 */
define(["backbone",
        'Events',
        "views/Login",
        'views/Wrapper', 'views/search/Search', "views/Card", "views/CardEdit"],
    function(Backbone, events,
             LoginView,
             wrapper, SearchView, CardView, CardEditView) {

        var login = function() {
            console.log("show login view");
            var search = new SearchView();
            wrapper.show(search);
        };


        var routes = new (Backbone.Router.extend({
            routes:{
                "": login,
                "films/:id":"details",
                "card": "card",
                "card-edit": "cardEdit"
            },
            details:function(id){
                //$$("details").show();
                //$$("details").parse(borgs.get(id).attributes);
            },

            card: function() {
                var card = new CardView();
                wrapper.show(card);
            },

            cardEdit: function() {
                wrapper.show(new CardEditView);
            }
        }));
        Backbone.history.start();

        events.on("route.login", login);
        events.on("route.employeeSearch", function() {
            EmployeeSearch.open();
        });



        //return router;

});