/**
 * Created by DMitin on 17.12.2015.
 */
define(['marionette'], function(Marionette) {

    var Wrapper = Marionette.Region.extend({
        el: ".wrapper"
    });

    var wrapper = new Wrapper();
    return wrapper;
});