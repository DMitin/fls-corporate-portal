/**
 * Created by DMitin on 25.12.2015.
 */
define(["marionette",
        'views/search/SearchItem'],
    function(Marionete, SearchItem) {

        var CollectionView = Marionette.CollectionView.extend({
            //el: ".search-results",
            childView: SearchItem
        });
        return CollectionView;
});