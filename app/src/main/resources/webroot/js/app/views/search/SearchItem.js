/**
 * Created by DMitin on 25.12.2015.
 */
define(["backbone", "marionette",
    'text!templates/search-item.html'],
    function(Backbone, Marionette, template) {


        /*
         <div class="personal-info">
            <div class="name">������� ���� ����������</div>
            <div class="position">Front-end �����������</div>
            <div class="email"><span>email</span> <a href="mailto:ivan.andreev@firstlinesoftware.com">ivan.andreev@firstlinesoftware.com</a></div>
            <div class="skype"><span>skype</span> ivan.andreev</div>
         </div>
         <div class="align-center map">
            <a class="icn-map-marker"><u>222-�</u></a>
         </div>
         */

        var Item = Marionette.ItemView.extend({
            className: "search-item",
            template: template
        });

        return Item;
});