/**
 * Created by DMitin on 17.12.2015.
 */
define(["marionette", "text!templates/card.html"],
    function(Marionette, cardTemplate) {

    var Card = Marionette.ItemView.extend({
        className: "content",
        template: cardTemplate
    });

    return Card;
});