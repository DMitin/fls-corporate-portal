/**
 * Created by DMitin on 17.12.2015.
 */
define(['marionette',
    'text!templates/card-edit.html'],
    function(Marionette, tmpl) {

        var CardEdit = Marionette.ItemView.extend({
            className: "content edit-view",
            template: tmpl
        });

        return CardEdit;
});