/**
 * Created by DMitin on 02.12.2015.
 */
define(["marionette", "text!templates/found-user.html"],
        function(Marionette, foundUser) {
    var FoundUser = Marionette.ItemView.extend({
        template: foundUser,

        events: {
            "click a": "openUserProfile"
        },

        openUserProfile: function() {
            console.log("openUserProfile");
            this.trigger("openUserProfile", this.model.get("id"));
        }
    });

    return FoundUser;
});