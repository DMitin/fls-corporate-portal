/**
 * Created by DMitin on 25.12.2015.
 */
define(["marionette", "backbone.select",
        "collections/PaginatorCollection", "collections/SearchUserCollection",
        "text!templates/search-pagination.html", "text!templates/search-pagination-end.html"],
    function(Marionette, backboneSelect,
             pCollection, searchUserCollection,
             template, templateEnd) {

        var PageView = Marionette.ItemView.extend({
            tagName: "li",
            template: '<a class="page">{{number}}</a>',
            triggers: {
                "click a": "go:page"
            },
            ui: {
                a: "a"
            },
            initialize: function() {
                this.listenTo(this.model, "selected", function() {
                   this.select();
                });
                this.listenTo(this.model, "deselected", function() {
                    this.deselect();
                });
            },

            select: function() {
                this.ui.a.addClass("active");
            },
            deselect: function() {
                this.ui.a.removeClass("active");
            }
        });

        var SummaryView = Marionette.ItemView.extend({
            tagName: "li",
            className: "pull-right",
            template: '{{start}} - {{stop}} из {{all}}',
            modelEvents: {
                'change': 'render'
            },
        });


        //var View = Marionette.CompositeView.extend({
        var View = Marionette.CollectionView.extend({

            //template: false,
            collection: pCollection,
            ///childView: ChildView,
            getChildView: function(item) {
                if (item.get("summary")) {
                    return SummaryView;
                } else {
                    return PageView;
                }
            },
            userCollection: searchUserCollection,

            ui: {
                fullLeft: ".icn-angle-double-left",
                left: ".icn-angle-left"
            },

            onChildviewGoPage: function(e, obj) {
                this.collection.select(obj.model);
                var pageNumber = obj.model.get("number");
                this.userCollection.getPage(pageNumber);
                var summaryModel =  this.collection.at(this.collection.length - 1); // всегда идёт последней

                var pageSize = this.userCollection.state.pageSize;
                var totalRecords = this.userCollection.state.totalRecords;
                var last = totalRecords < (pageSize * pageNumber)? totalRecords: pageSize;
                summaryModel.set("start", (pageNumber -1) * this.userCollection.state.pageSize);
                summaryModel.set("stop", last);
            },

            //childViewContainer: ".icn-angle-left",


            events: {
               "click @ui.fullLeft": "fullLeft"
                //"click .page": "goToPage"
            },


            fullLeft: function() {
                console.log("fullLeft");
            },

            goToPage: function() {

            }
        });

        return View;
});