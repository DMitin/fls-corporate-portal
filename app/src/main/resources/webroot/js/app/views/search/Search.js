/*
    Главное View для поиска
 */
define(['marionette',
    "collections/SearchUserCollection",
    //'services/search',
    'views/search/Pagination', 'views/search/SearchItemCollection', 'views/search/SearchItem',
    'text!templates/search.html'
], function(Marionette, //searchService,
            searchUserCollection,
            Pagination, SearchItemCollection, SearchItem,
            searchTemplate) {

    var SearchView = Marionette.ItemView.extend({
        className: "content",
        template: searchTemplate,

        ui: {
            // строчка ввода поиска
            searchText: ".search input",
            // здесь показфывются найденные сотрудники
            searchResults: ".search-results",
            // страницы поиска
            pagination: ".pagination ul"
        },

        events: {
            "keyup  @ui.searchText": function(e) {
                if(e.which == 13) {
                    var text = this.ui.searchText.val();
                    //searchService.searchEmployees(text);
                    searchUserCollection.search(text);
                }
            }
        },

        onRender: function() {
/*
            var employee = {
                firstName: "Денис",
                lastName: "Митин",
                middleName: "Юрьюевич",
                position: "Front-end разработчик",
                email: "ivan.andreev@firstlinesoftware.com",
                skype: "ivan.andreev"
            };
            var collection = new Backbone.Collection([
                employee, employee]);
*/

            var items = new SearchItemCollection({
                el: this.ui.searchResults,
                collection: searchUserCollection
            });
            items.render();

            var pagination = new Pagination({
                el: this.ui.pagination
            });
            pagination.render();

            /*
            var paginator = new Backgrid.Extension.Paginator({
                collection: searchUserCollection
            });
            */


            //$("#paginator").append(paginator.render().$el);
        }
    });

    return SearchView;
});