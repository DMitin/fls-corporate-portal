define(['backbone', 'marionette', 'handlebars',
        'Events',
        'text!templates/login.html'],

    function (Backbone, Marionette, Handlebars, events, template) {
        'use strict';

        console.log("init Login view");
        console.log(template);

        var LoginView = Marionette.ItemView.extend({

            template: template,

            // View Event Handlers
            events: {
                "click button": "login"
            },

            login: function(e) {
                e.preventDefault();
                console.log("login");
                events.employeeSearch();
            }
        });

        // Returns the View class
        return LoginView;

    }
);
