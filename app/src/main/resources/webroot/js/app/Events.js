/**
 * Created by DMitin on 04.12.2015.
 */
define(['backbone'], function(backbone) {
    var api = {
        login: function() {
            this.trigger("route.login");
        },
        employeeSearch: function() {
            this.trigger("route.employeeSearch");
        }
    };
    _.extend(api, Backbone.Events);

    return api;


});