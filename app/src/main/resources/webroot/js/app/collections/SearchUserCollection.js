/**
 * Created by DMitin on 02.12.2015.
 */
define(["backbone", "backbone.paginator",
        "collections/PaginatorCollection",
        "models/SearchUserModel"],
    function(Backbone, paginator,
             paginatorCollection,
             SearchUser) {

        var allKey = "_!all_";
        var paginatorCollection = paginatorCollection;

        //var SearchUserCollection = Backbone.Collection.extend({
        var SearchUserCollection = Backbone.PageableCollection.extend({
            model: SearchUser,
            url: function(options) {
                var param;
                if (this.searchParam) {
                    param = this.searchParam;
                } else {
                    param = allKey;
                }
                return "rest/employee/search/" + param
            },

            search: function(param) {
                this.searchParam = param;
                this.fetch({success: function(collection){

                    //console.log("�������� ��������� �������");
                    var totalPages = collection.state.totalPages;
                    var totalRecords = collection.state.totalRecords;
                    var pageSize = collection.state.pageSize;
                    paginatorCollection.setPageSize(totalPages, totalRecords, pageSize);
                }});
            },

            mode: "client"
        });
        var collection = new SearchUserCollection();
        collection.setPageSize(10);
        return collection;
});