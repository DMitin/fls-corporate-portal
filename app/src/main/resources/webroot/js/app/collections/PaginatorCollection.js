/**
 * Created by DMitin on 27.12.2015.
 */
define(["backbone", "backbone.select"],
    function(Backbone, select) {

        var defSummary = {
            summary: true,
            start: 0,
            stop: 0,
            all: 0
        };

        var PaginatorModel = Backbone.Model.extend({
            initialize: function () {
                Backbone.Select.Me.applyTo( this );
            }
        });


        var Collection = Backbone.Collection.extend({

            model: PaginatorModel,

            initialize: function ( models ) {
                Backbone.Select.One.applyTo( this, models );
            },

            setPageSize: function(totalPages, totalRecords, pageSize) {

                var elems = [];
                if (!totalPages) {
                    elems.push(defSummary);
                } else if (totalPages > 0) {
                    for (var i = 0; i < totalPages; i ++) {
                        elems.push({
                            number: i + 1
                        });
                    }
                    // если вернулось меньше чем на одной странице - то возвращаем totalRecords
                    var last = totalRecords < pageSize? totalRecords: pageSize;
                    elems.push({
                        summary: true,
                        start: 0,
                        stop: last,
                        all: totalRecords
                    });
                }
                this.reset(elems);
                this.at(0).select();
                console.log("size = " + totalPages);
            }
        });

        return new Collection([defSummary]);
});