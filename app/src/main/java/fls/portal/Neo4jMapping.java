package fls.portal;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class Neo4jMapping {

	public static JsonObject mapSingleEmployee(JsonObject employeeJson) {
		if (employeeJson.getJsonArray("data").size() != 1) {
			throw new IllegalArgumentException();
		}
		JsonArray employeeData = employeeJson
				.getJsonArray("data")
					.getJsonArray(0);

		return mapEmployeeJson(employeeData);
	}


	public static JsonArray mapEmployees(Object employeeJson) {
		JsonObject o = (JsonObject) employeeJson;
		return mapEmployees(o);
	}
	public static JsonArray mapEmployees(JsonObject employeeJson) {
		JsonArray employeeData = employeeJson.getJsonArray("data");
		JsonArray arr = new JsonArray();
		for (int i = 0; i < employeeData.size(); i ++) {
			JsonArray internalArray = employeeData.getJsonArray(i);
			JsonObject mappedEmployeeData = mapEmployeeJson(internalArray);
			arr.add(mappedEmployeeData);
		}
		return  arr;
	}

	public static JsonObject mapEmployeeJson(JsonArray employeeJson) {
		JsonObject metadata = employeeJson
				.getJsonObject(0)
				.getJsonObject("metadata");

		JsonObject data = employeeJson
				.getJsonObject(0)
				.getJsonObject("data");

		JsonObject response = new JsonObject()
				.put("metadata", metadata)
				.put("data", data);

		return  response;
	}
/*
	public static JsonObject mapSingleEmployee(String employeeString) {
		JsonObject o = new JsonObject(employeeString);
		return mapSingleEmployee(o);
	}
*/

	public static JsonObject mapSingleEmployee(Object employeeObject) {
		//String str = (String) employeeObject;
		JsonObject o = (JsonObject) employeeObject;
		return mapSingleEmployee(o);
	}
}
