package fls.portal;

import io.vertx.core.json.JsonObject;

public class Warnings {

	public static final JsonObject ERROR_GET_OBJECT_BY_ID = new JsonObject()
			.put("status", "WARNING")
			.put("code", 1)
			.put("message", "Не могу найти объект по id");

	public static final JsonObject AUTH_ERROR = new JsonObject()
			.put("status", "ERROR")
			.put("code", 2)
			.put("message", "Пользователь не авторизован");

	public static final JsonObject NOT_PERMISSIONS_TO_CREATE_USER = new JsonObject()
			.put("status", "ERROR")
			.put("code", 3)
			.put("message", "Нельзя создать/изменить/удалить пользователя, залогинившись под другим именем");

	public static final JsonObject LOGGED_USER_NOT_FOUND = new JsonObject()
			.put("status", "WARNING")
			.put("code", 4)
			.put("message", "Авторизован. Данные пользователя не найдены в базе портала. Создайте нового сотрудника");

	public static final JsonObject CANNOT_SAVE_EMPLOYEE_WITHOUT_AUTHNAME = new JsonObject()
			.put("status", "ERROR")
			.put("code", 5)
			.put("message", "Не могу Сохранить сотрудника у которогно нет authName");

	public static final JsonObject ERROR_CREATE_USER_ES = new JsonObject()
			.put("status", "ERROR")
			.put("code", 6)
			.put("message", "Произошла ошибка при создании пользователя. Ошибка ElasticSearch");
}
