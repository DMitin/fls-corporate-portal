package fls.portal.exception;

import javax.validation.ConstraintViolation;
import java.util.Set;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 04.11.2015
 * Time: 13:55
 */
public class ValidationException extends RuntimeException {
    private Set<ConstraintViolation<Object>> violations;

    public ValidationException(Set<ConstraintViolation<Object>> violations) {
        this.violations = violations;
    }

    public ValidationException(String message, Set<ConstraintViolation<Object>> violations) {
        super(message);
        this.violations = violations;
    }

    public Set<ConstraintViolation<Object>> getViolations() {
        return violations;
    }
}
