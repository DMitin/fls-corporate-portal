package fls.portal;

import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.Message;
import io.vertx.core.eventbus.ReplyException;

public class Utils {

	public static Integer mapStringToInt(String s) {
		Integer employeeId = null;
		try {
			employeeId = Integer.valueOf(s);
		} catch (NumberFormatException e) {}
		return employeeId;
	}

	public static String getFailMessage(AsyncResult<Message<Object>> resp) {
		ReplyException t = (ReplyException)resp.cause();
		return t.getMessage();
	}
}
