package fls.portal.controller;

import io.vertx.core.eventbus.EventBus;
import io.vertx.ext.web.Router;

/**
 * Created by PGaev on 16.10.2015.
 * Высокоуровневый интерфейс для маппинга http (REST) запросов - на события vert.x eventBus  
 */
public interface VerticleController {
    void deploy(Router router, EventBus eb);

    /**
     * Получить корневой url, по которому будут маппиться http запросы
     * @return
     */
    String getBasePath();

    /**
     * Получить имя события, на которое будут маппиться http запросы
     * @return
     */
    String getBaseEvent();
}
