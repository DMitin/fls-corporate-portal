package fls.portal.controller.impl;

import fls.portal.controller.AbstractRestController;
import fls.portal.model.Project;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 04.11.2015
 * Time: 11:54
 */
public class ProjectRestController extends AbstractRestController<Project> {


    @Override
    protected Class<Project> getObjectClass() {
        return Project.class;
    }
}
