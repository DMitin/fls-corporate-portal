package fls.portal.controller;

import fls.portal.exception.ValidationException;
import fls.portal.model.Identifiable;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Set;

import static fls.portal.consumer.CRUDEventConsumer.*;

/**
 * Created by PGaev on 16.10.2015.
 */
public abstract class AbstractRestController<T extends Identifiable> extends AbstractController implements VerticleController {


    public static final String ID_PARAM = "id";
    private static final String ID_PATH = ":".concat(ID_PARAM);

    public static final String SEARCH_PARAM = "searchString";
    public static final String SEARCH_SUB_URL = "search/";
    private static final String SEARCH_PATH = ":".concat(SEARCH_PARAM);

    private Validator validator;

    @Override
    public void deploy(Router router, EventBus eb) {

        deploySearch(router, eb);
        deployCreate(router, eb);
        deployGetById(router, eb);
        deployUpdate(router, eb);
        deployDelete(router, eb);
    }

    private void deploySearch(Router router, EventBus eb) {
        System.out.println("deploy: " + getBasePath());
        deploy(router, eb, getSearchPath(), HttpMethod.GET, rc -> eb.send(getSearchEvent(), getSearchStr(rc), eventResponse -> {
            processEventResponse(rc, eventResponse);
        }));
    }

    protected void deployCreate(Router router, EventBus eb) {
        deploy(router, eb, getBasePath(), HttpMethod.POST, rc -> {
            validate(rc);
            eb.send(getCreateEvent(), getRequestObject(rc), eventResponse -> {
                processEventResponse(rc, eventResponse);
            });
        });
    }

    private void deployGetById(Router router, EventBus eb) {
        System.out.println(getIdPath());
        deploy(router, eb, getIdPath(), HttpMethod.GET, rc -> {
            Long id = getId(rc);
            eb.send(getGetByIdEvent(), id, eventResponse -> {
                processEventResponse(rc, eventResponse);
            });
        });
    }

    private void deployUpdate(Router router, EventBus eb) {
        deploy(router, eb, getIdPath(), HttpMethod.PUT, rc -> {
            Long id = getId(rc);
            validate(rc);
            JsonObject editMessage = new JsonObject()
                    .put(ID_PARAM, id)
                    .put("editData", getRequestObject(rc));
            eb.send(getUpdateEvent(), editMessage, eventResponse -> {
                processEventResponse(rc, eventResponse);
            });
        });
    }

    private void deployDelete(Router router, EventBus eb) {
        deploy(router, eb, getIdPath(), HttpMethod.DELETE, rc -> eb.send(getDeleteEvent(), getId(rc), eventResponse -> {
            processEventResponse(rc, eventResponse);
        }));
    }

    protected abstract Class<T> getObjectClass();

    @Override
    public String getBasePath() {
        return getObjectClass().getSimpleName().toLowerCase();
    }

    @Override
    public String getBaseEvent() {
        return getObjectClass().getSimpleName();
    }


    protected String getSearchEvent() {
        return getBaseEvent().concat(SEARCH_SUFFIX);
    }

    protected String getCreateEvent() {
        return getBaseEvent().concat(CREATE_SUFFIX);
    }

    protected String getGetByIdEvent() {
        return getBaseEvent().concat(GET_BY_ID_SUFFIX);
    }

    protected String getUpdateEvent() {
        return getBaseEvent().concat(UPDATE_SUFFIX);
    }

    protected String getDeleteEvent() {
        return getBaseEvent().concat(DELETE_SUFFIX);
    }

    protected String getIdPath() {
        return getBasePath().endsWith("/") ? getBasePath().concat(ID_PATH) : getBasePath().concat("/" + ID_PATH);
    }

    protected String getSearchPath() {
        if (getBasePath().endsWith("/")) {
            return getBasePath().concat(SEARCH_SUB_URL).concat(SEARCH_PATH);
        } else {
            return getBasePath().concat("/" + SEARCH_SUB_URL).concat(SEARCH_PATH);
        }
    }

    protected Long getId(RoutingContext rc) {
        return Long.valueOf(rc.request().getParam(ID_PARAM));
    }

    protected String getSearchStr(RoutingContext rc) {
        String raw = rc.request().getParam(SEARCH_PARAM);
        String coded = null;
        try {
            coded = URLDecoder.decode(raw, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return coded;
    }


    protected Object getRequestObject(RoutingContext rc) {
        return rc.getBodyAsJson();
    }

    protected void validate(RoutingContext rc) {
        T obj = Json.decodeValue(rc.getBodyAsString(), getObjectClass());
        Set<ConstraintViolation<Object>> validationErrors = getValidator().validate(obj);
        if (!validationErrors.isEmpty()) {
            throw new ValidationException(validationErrors);
        }
    }

    protected Validator getValidator() {
        if (validator == null) {
            initValidator();
        }
        return validator;
    }

    synchronized private void initValidator() {
        if (validator != null) {
            return;
        }
//        ValidatorFactory hibernateVF = Validation.byProvider(HibernateValidator.class)
//                .configure().buildValidatorFactory();
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }
}
