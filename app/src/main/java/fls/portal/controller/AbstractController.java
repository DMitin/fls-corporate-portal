package fls.portal.controller;

import fls.portal.Warnings;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.buffer.impl.BufferImpl;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

/**
 * Created by PGaev on 16.10.2015.
 */
public abstract class AbstractController implements VerticleController {

    protected void processEventResponse(RoutingContext rc, AsyncResult<Message<Object>> eventResponse) {
        rc.response().putHeader("Content-type", "application/json; charset=utf-8");
        if (eventResponse.failed()) {
            rc.fail(eventResponse.cause());
            return;
        }
        Object result = eventResponse.result().body();
        if (result == null) {
            return;
        }
        if (result instanceof JsonObject) {
            rc.response().end(result.toString());
        } else if (result instanceof String) {
            rc.response().end((String) result);
        } else if (result instanceof BufferImpl) {
            rc.response().end(result.toString());
        } else if (result instanceof JsonArray) {
            rc.response().end(result.toString());
        } else {
            throw new IllegalStateException("Unsupported result class: " + result.getClass().getName());
        }
    }

    protected String getPath(String path) {
        return path.startsWith("/") ? path : "/".concat(path);
    }

    final protected void deploy(Router router, EventBus eb, String path, HttpMethod method, Handler<RoutingContext> requestHandler) {
        Route route = router.route().path(getPath(path));
        route.method(method);
        route.handler(requestHandler);
    }
}
