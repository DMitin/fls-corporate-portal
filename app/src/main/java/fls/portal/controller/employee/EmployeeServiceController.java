package fls.portal.controller.employee;

import fls.portal.Neo4j;
import fls.portal.Neo4jMapping;
import fls.portal.consumer.AbstractCRUDConsumer;
import fls.portal.consumer.CRUDEventConsumer;
import fls.portal.model.Employee;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

/**
 * Created by DMitin on 22.10.2015.
 */
public class EmployeeServiceController /*extends AbstractVerticle*/ extends AbstractCRUDConsumer<Employee> {

	 private static final String SEARCH_QUERY_TEMPLATE = "MATCH (e:Employee) " +
             "WHERE  e.searchString CONTAINS {search} " +
             "RETURN e " +
             "ORDER BY e.firstName, e.lastName";

    @Override
    protected Class<Employee> getModelClass() {
        return Employee.class;
    }

    @Override
    public void deploy(EventBus eventBus) {
        super.deploy(eventBus);

        eventBus.consumer(getBaseEvent() + CRUDEventConsumer.SEARCH_SUFFIX, searchEmployee -> {
            String searchString = (String)searchEmployee.body();
            if (searchString.equals(Neo4j.ALL_KEY_SEARCH)) {
                searchString = "";
            } else {
                searchString = searchString.toLowerCase();
            }
            System.out.println("employee-search");

            JsonObject message = new JsonObject()
                    .put("query", SEARCH_QUERY_TEMPLATE)
                    .put("params", new JsonObject()
                                        .put("search", searchString));
            eventBus.send(Neo4j.EVENT_NAME, message, cypherResponse -> {
                searchEmployee.reply(Neo4jMapping.mapEmployees(
                                cypherResponse.result().body())
                );
            });
            /*
            eventBus.send("es.searchEmployee", searchString, esResp -> {
                searchEmployee.reply(esResp.result().body());
            });
            */
        });
    }

	@Override
	public boolean isSearchable() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public String getIndexString(JsonObject data) {
		String fn = data.getString("firstName");
		String ln = data.getString("lastName");
		return (fn + ln).toLowerCase();
	}


}
