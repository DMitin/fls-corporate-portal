package fls.portal.controller.employee;

import fls.portal.controller.AbstractRestController;
import fls.portal.model.Employee;

/**
 * Created by DMitin on 22.10.2015.
 */
public class EmployeeRestController extends AbstractRestController<Employee> {

    @Override
    protected Class<Employee> getObjectClass() {
        return Employee.class;
    }
}
