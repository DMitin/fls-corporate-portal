package fls.portal;

import io.vertx.core.AbstractVerticle;
public class TestServer extends AbstractVerticle {
  public void start() {
    vertx.createHttpServer().requestHandler(req -> {
      req.response()
        .putHeader("content-type", "text/plain")
        .end("Hello from Vert.x4!");
    }).listen(8081);
  }
}
