package fls.portal;

import io.vertx.core.json.JsonObject;

public class Errors {

	public static final JsonObject ERROR_PARSE_ID = new JsonObject()
			.put("status", "ERROR")
			.put("code", 1)
			.put("message", "Значение id не является числом");
}
