package fls.portal.util;

import java.util.Base64;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;

public class ClearNeo4j extends AbstractVerticle {

	@Override
	public void start() {
		
		DeploymentOptions neo4jOptions = new DeploymentOptions()
				.setConfig(config().getJsonObject("neo4j"));
		
		vertx.deployVerticle("fls.portal.Neo4j", neo4jOptions, this::cleanNeo4j);
		
		
	}
	
	public void cleanNeo4j(AsyncResult<String> completionHandler) {
		
		System.out.println("Начинаю удаление");
		
		String deleteAllRequest = "MATCH (n) OPTIONAL MATCH (n)-[r]-() DELETE n,r";
		JsonObject deleteAll = new JsonObject()
				 .put("query", deleteAllRequest)
				 .put("params", new JsonObject());
		
		vertx.eventBus().send("neo4j.runCypher", deleteAll, cypherResponse -> {
			 
			if (cypherResponse.succeeded()) {	
				 System.out.println("Очистил всё");
			 } else {
				 System.out.println("ошибка");
			 }
		});
	}
	
	
}
