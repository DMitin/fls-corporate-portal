package fls.portal.util;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;

public class CreateNeo4jConstraints extends AbstractVerticle {

	String searchStringIndex = "CREATE INDEX ON :Employee(searchString)";
	@Override
	public void start() {

		DeploymentOptions neo4jOptions = new DeploymentOptions()
				.setConfig(config().getJsonObject("neo4j"));

		vertx.deployVerticle("fls.portal.Neo4j", neo4jOptions, this::createConstrains);


	}

	public void createConstrains(AsyncResult<String> completionHandler) {
		System.out.println("Create constraints");


		String authNameUnique = "CREATE CONSTRAINT ON (e:Employee) ASSERT e.authName IS UNIQUE";
		String searchStringIndex = "CREATE INDEX ON :Employee(searchString)";
		addConstraint(authNameUnique, authNameResponse -> {
			if (authNameResponse.succeeded()) {
				System.out.println(authNameUnique + " : OK! created!");
			} else {
				System.out.println(authNameUnique + "ERROR!!!");
			}

			addConstraint(searchStringIndex, searchResponse -> {
				if (searchResponse.succeeded()) {
					System.out.println(searchStringIndex + " : OK! created!");
				} else {
					System.out.println(searchStringIndex + "ERROR!!!");
				}
			});

		});

	}

	public void addConstraint(String text, Handler<AsyncResult<Message<JsonObject>>>  handler) {
		JsonObject queryJson = new JsonObject()
				.put("query", text)
				.put("params", new JsonObject());
		vertx.eventBus().send("neo4j.runCypher", queryJson, handler);
	}
}
