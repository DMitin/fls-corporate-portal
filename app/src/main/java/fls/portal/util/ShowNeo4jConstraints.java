package fls.portal.util;

import java.util.Base64;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpClientResponse;
import io.vertx.core.json.JsonObject;

public class ShowNeo4jConstraints extends AbstractVerticle {

	@Override
	public void start() {
		System.out.println("Изменение пароля neo4j");
		
		JsonObject neo4jOptions = config().getJsonObject("neo4j");
		
		String host = neo4jOptions.getString("neo4j.host");
		System.out.println("neo4j хост: " + host);
		
		int port = neo4jOptions.getInteger("neo4j.port");
		System.out.println("neo4j порт: " + port);
		
		String userName = neo4jOptions.getString("neo4j.user");
		System.out.println("имя пользователя: " + userName);
		
		String initPass = neo4jOptions.getString("neo4j.initPassword");
		System.out.println("пароль по умолчанию: " + initPass);
		
		String newPass = neo4jOptions.getString("neo4j.newPassword");
		System.out.println("новый пароль: " + newPass);
		
		
		String authStr = userName + ":" + newPass;
		Base64.Encoder e = Base64.getEncoder();
		String encoded = e.encodeToString(authStr.getBytes());
		 
		HttpClientOptions options = new HttpClientOptions()
			.setDefaultPort(port)
			.setDefaultHost(host);
		
		HttpClient neo4j = vertx.createHttpClient(options);

		Handler<HttpClientResponse> responseHandler = (resp) -> {

			Buffer totalBuffer = Buffer.buffer();
			resp.handler(buffer -> {
				totalBuffer.appendBuffer(buffer);
			});


			resp.endHandler(v -> {
				int statusCode = resp.statusCode();
				if (statusCode == 200) {
					System.out.println("Успешено ролучены constraints: ");
					System.out.println(totalBuffer);
				} else {
					System.out.println("ОШИБКА!!! при получении constrains");
				}

				vertx.close(h -> {
					System.out.println("Завершение работы");
					Runtime.getRuntime().halt(0);
				});
			});
		};

		HttpClientRequest request = neo4j.get("/db/data/schema/index/Employee", responseHandler);
		
		JsonObject body = new JsonObject();
		body.put("password", newPass);
		request.putHeader("Authorization", "Basic " + encoded);
		request.end(body.toString());
	}
}
