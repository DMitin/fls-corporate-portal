package fls.portal;

import java.net.URLEncoder;
import java.util.Formatter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.json.JsonObject;

public class ElasticSearch extends AbstractVerticle {

	@Override
	public void start() {
		
		System.out.println("START ELASTICSEARCH VERTICLE");
		String esEmployeePath = "/portal/employee/";
		
		String host = config().getString("es.host");
		System.out.println("elasticsearch хост: " + host);
		
		int port = config().getInteger("es.port");
		System.out.println("elasticsearch порт: " + port);
		
		HttpClientOptions options = new HttpClientOptions()
				 .setDefaultPort(port)
				 .setDefaultHost(host);
			 
		EventBus eb = vertx.eventBus();
		HttpClient elasticsearch = vertx.createHttpClient(options);
		
		
		/*
		 * Проверка подключения к neo4j при запуске
		 */
		HttpClientRequest connectRequest = elasticsearch.get("/", resp-> {
			resp.bodyHandler(totalBuffer -> {
				// Now all the body has been read
				int status = resp.statusCode();
				if (status == 200) {
					System.out.println("Подключено к neo4j");
					System.out.println("Ответ сервера: " + totalBuffer);
				} else {
					System.out.println("Не удалось подключиться к neo4j");
					System.out.println("Ответ сервера: " + totalBuffer);
				}				
			});
		});
		
		connectRequest.end();
		
		
		 /*
		  * Добавление данных сотрудника для full text search
		  */
		eb.consumer("es.addEmployee", esAddEmployee -> {
			System.out.println("es.addEmployee");

			JsonObject data = (JsonObject) esAddEmployee.body();
			Integer neo4jId = data.getInteger("neo4jId");
			if (neo4jId == null) {
				esAddEmployee.fail(500, "Нельзя сохранить сотрудника в es без neo4jId");
			}
			String url = esEmployeePath + neo4jId;
			HttpClientRequest request = elasticsearch.put(url, resp->{
				resp.bodyHandler(totalBuffer -> {
					esAddEmployee.reply(totalBuffer);
				});
			});
				 
			 
			 request.end(data.toString());
		});
		 
		 /*
		  * Поиск сотрудника по имени фамилии отчеству
		  */
		eb.consumer("es.searchEmployee", esSearchEmployee -> {
			String searchdata =  (String) esSearchEmployee.body();
			System.out.println("search - " + searchdata);
			 
			HttpClientRequest request = elasticsearch.post("/_search?q=" + searchdata, resp->{
				resp.bodyHandler(totalBuffer -> {
					System.out.println(totalBuffer.toString());
					System.out.println("ELASTCSEARCH: SEARCH EMPLOYEE");
					System.out.println("searchString: " + searchdata);
					JsonObject esJsonResponse = new JsonObject(totalBuffer.toString());
					
					esSearchEmployee.reply(esJsonResponse.getJsonObject("hits"));					 
				});
			});
				 
			request.end();
		});
		 
		 
		/*
		 * Получение сотрудника по id (в основном для тестов)
		 * сотрудник сохраняется в es с id который пришёл из neo4j
		 */
		eb.consumer("es.getUserById", esGetEmployeeById -> {
			String id =  (String) esGetEmployeeById.body();
			System.out.println("ES: getUserById: " + id);
			 
			String url = esEmployeePath + id;
			HttpClientRequest request = elasticsearch.get(url, resp->{
				resp.bodyHandler(totalBuffer -> {
					System.out.println(totalBuffer.toString());
					 
					Formatter fm = new Formatter();
					JsonObject esJsonResponse = new JsonObject(totalBuffer.toString());
					System.out.format("ES: getUserById (%s): response:", id);
					System.out.println(esJsonResponse);
					esGetEmployeeById.reply(esJsonResponse);
				});
			});
				 
			request.end();
		});
		 
		 /*
		  * Обновить сотрудника
		  */
		eb.consumer("es.updateEmployee", esUpdateEmployee -> {

			JsonObject updateEmployeeMessage = (JsonObject) esUpdateEmployee.body();
			Integer id = updateEmployeeMessage.getInteger("id");

			System.out.println("ES: updateEmployee" + id);
			JsonObject employeeData = updateEmployeeMessage.getJsonObject("employeeData");
			String url = esEmployeePath + id;

			// TODO сделать чтоб кидалась ошибка, если такого сотрудника нет
			HttpClientRequest request = elasticsearch.put(url, resp -> {
				resp.bodyHandler(totalBuffer -> {
					System.out.println("ES: updateEmployee response " + totalBuffer);
					esUpdateEmployee.reply(totalBuffer);
				});
			});

			request.end(employeeData.toString());
		});
		 
		 
		/*
		 * Удалить сотрудника
		 */
		eb.consumer("es.deletyEmployee", esDeleteEmployee -> {
			 
			Integer employeeId = (Integer) esDeleteEmployee.body();
			System.out.println("ES: deleteEmployee" + employeeId);
			String url = esEmployeePath + employeeId;
			 
			HttpClientRequest request = elasticsearch.delete(url, resp->{
				resp.bodyHandler(totalBuffer -> {
					System.out.println("ES: updateEmployee response " + totalBuffer);
					esDeleteEmployee.reply(totalBuffer);
				});
			});
				 
			request.end();
		});
		
		 
	}
}
