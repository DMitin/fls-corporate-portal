package fls.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 04.11.2015
 * Time: 11:45
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project implements Identifiable {

    private Long id;

    @NotNull
    private String name;
    @NotNull
    private String description;

    private Date startDate;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
