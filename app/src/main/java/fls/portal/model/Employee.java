package fls.portal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 04.11.2015
 * Time: 15:59
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Employee implements Identifiable {
    private Long id;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
