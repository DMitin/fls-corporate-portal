package fls.portal.model;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 04.11.2015
 * Time: 16:19
 */
public interface Identifiable {
    Long getId();
}
