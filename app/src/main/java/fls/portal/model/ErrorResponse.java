package fls.portal.model;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;
import java.util.List;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 04.11.2015
 * Time: 14:05
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorResponse {
    private Integer code;
    private String error;
    private String message;
    private List details;

    public ErrorResponse(Integer code) {
        this(code, null, null, null);
    }

    public ErrorResponse(Integer code, String error) {
        this(code, error, null, null);
    }

    public ErrorResponse(Integer code, String error, String message) {
        this(code, error, message, null);
    }

    public ErrorResponse(Integer code, String error, String message, List details) {
        this.code = code;
        this.error = error;
        this.message = message;
        this.details = details;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List getDetails() {
        return details;
    }

    public void setDetails(List details) {
        this.details = details;
    }
}
