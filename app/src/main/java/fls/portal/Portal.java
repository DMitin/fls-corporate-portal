package fls.portal;

import fls.portal.auth.AjaxLoginHandler;
import fls.portal.auth.CheckUserHandler;
import fls.portal.auth.FakeAuthProvider;
import fls.portal.auth.SimpleLDAPAuthProvider;
import fls.portal.consumer.CRUDEventConsumer;
import fls.portal.consumer.impl.ProjectEventConsumer;
import fls.portal.controller.VerticleController;
import fls.portal.controller.employee.EmployeeRestController;
import fls.portal.controller.employee.EmployeeServiceController;
import fls.portal.controller.impl.ProjectRestController;
import fls.portal.handler.CommonExceptionHandler;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.*;
import io.vertx.ext.web.sstore.LocalSessionStore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

//import java.util.Base64;

public class Portal extends AbstractVerticle {

	private static final List<VerticleController> controllers;

	private static final List<CRUDEventConsumer> consumers;

	private static final Logger LOGGER = LoggerFactory.getLogger(Portal.class);

	static {
		controllers = Collections.unmodifiableList(Arrays.asList(
				new EmployeeRestController()
				, new ProjectRestController()
		));
	}

	static {
		consumers = Collections.unmodifiableList(Arrays.asList(
				new EmployeeServiceController(),
				new ProjectEventConsumer()

		));
	}	
	
	public static final String FAKE_AUTH = "fake";


	@Override
	public void start() {

		String neo4jHost = config().getString("neo4j.host", "localhost");
		System.out.println(neo4jHost);

		DeploymentOptions neo4jOptions = new DeploymentOptions()
				.setConfig(config().getJsonObject("neo4j"));
		DeploymentOptions esOptions = new DeploymentOptions()
				.setConfig(config().getJsonObject("es"));


		vertx.deployVerticle("fls.portal.Neo4j", neo4jOptions);
		vertx.deployVerticle("fls.portal.Neo4jApi");
		//vertx.deployVerticle("fls.portal.ElasticSearch", esOptions);

/*
		JsonObject config = new JsonObject().put("keyStore", new JsonObject()
				.put("path", "keystore.jceks")
				.put("type", "jceks")
				.put("password", "secret"));
		AuthProvider provider = JWTAuth.create(vertx, config);

		DeploymentOptions authOptions = new DeploymentOptions().setWorker(true);
		vertx.deployVerticle(AuthController.class.getName(), authOptions);
*/



//		vertx.deployVerticle(EmployeeServiceController.class.getName());
//
		EventBus eb = vertx.eventBus();
		HttpServer server = vertx.createHttpServer();
		Router router = Router.router(vertx);

		Router restAPI = Router.router(vertx);

		restAPI.route().handler(CookieHandler.create());
		restAPI.route().handler(BodyHandler.create());
		restAPI.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));

        String authType = config().getString("auth.type", "fake");
        System.out.println("authType: " + authType);
        AuthProvider authProvider = null;
        if (authType.equals(FAKE_AUTH)) {
        	authProvider = new FakeAuthProvider(vertx);
        } else {
        	authProvider =  new SimpleLDAPAuthProvider(vertx);
        }


		restAPI.route().handler(UserSessionHandler.create(authProvider));
		restAPI.route("/private/*").handler(new CheckUserHandler(authProvider));
		restAPI.route("/private/*").handler(rc-> rc.response().end("private maethod access"));

		// проверка на создание Employee, что у создавамого объекта совпадает authName c
		// зарегестрированным пользователем
		/*
		restAPI.route("/employee/*")
				.method(HttpMethod.POST)
				.handler(this::checkPermissionsToCreateUser);
		// проверка на изменение/удаление пользователя, что id пользователя должен совпадать
		// с зарегестрированным пользователем
		restAPI.route("/employee/*")
				.method(HttpMethod.PUT)
				.method(HttpMethod.DELETE)
				.handler(this::checkPermissionsToAffectUser);
		restAPI.route("/login").handler(new AjaxLoginHandler(authProvider, this::successUserAuth));
		/*
		restAPI.route("/login").handler(rc-> {
			JsonObject body = rc.getBodyAsJson();
			System.out.println(body);
			rc.response().end();
		});
		*/
		restAPI.route("/logout").handler(context -> {
            context.clearUser();
            context.response().putHeader("location", "/").setStatusCode(302).end();
        });


		//router.route().handler(BodyHandler.create());
		router.route("/img/*").handler(StaticHandler.create("file-uploads"));

        router.route().failureHandler(new CommonExceptionHandler());

        /**
         * Маппинг URL'ов
         */
        for (VerticleController controller : controllers) {
            LOGGER.debug("deploying listener for path:" + controller.getBasePath());
            controller.deploy(restAPI,eb);
        }


        for (CRUDEventConsumer consumer : consumers) {
            LOGGER.debug("attaching consumer for base event:" + consumer.getGetByIdEvent());
            consumer.deploy(eb);
        }

        /**
         * Upload фотографии пользователя
         */
		Route uploadEmployeeAvatar = router.route().path("/form");
		uploadEmployeeAvatar.method(HttpMethod.POST);
		uploadEmployeeAvatar.handler(rc -> {
			Set<FileUpload> uploads = rc.fileUploads();
			FileUpload[] uploadsArray = uploads.toArray(new FileUpload[uploads.size()]);
			System.out.print(uploadsArray[0].uploadedFileName());
			rc.response().end("file upload");
		});

		 /*
		  * Получение сотрудника по id из ElasticSearch (только для тестов)
		  */
		restAPI.route()
				.path("/employee/es/:id")
				.method(HttpMethod.GET).handler(rc -> {

			rc.response().putHeader("Content-Type", "application/json");
			String id = rc.request().getParam("id");
			eb.send("es.getUserById", id, neo4jResponse -> {
				rc.response().putHeader("Content-type", "application/json; charset=utf-8");
				rc.response().end(neo4jResponse.result().body().toString());
			});
		});

		router.mountSubRouter("/rest", restAPI);
		router.route("/*").handler(StaticHandler.create());


		int port = config().getInteger("http.port", 8081);
		System.out.println("start server on port: " + port);
		server.requestHandler(router::accept).listen(port);
	}

	/**
	 * Обработка положительного результата авторизации
	 * Если пользователь найден в neo4j по authName, то посылаются данные пользователя
	 * В противном случае - посылается сообщение об ошибке
	 * @param rc
	 */
	public void successUserAuth(RoutingContext rc) {
		User user = rc.user();
    	String username = user.principal().getString("username");
    	SharedData sd = vertx.sharedData();
    	LocalMap<String, JsonObject> map = sd.getLocalMap("authUserData");
    	JsonObject userData = map.get(username); // если данные пользователя найдены, то они лежат здесь
    	if (userData != null) {
    		map.remove(username);
        	rc.response().end(userData.toString());
    	} else {
    		rc.response().end(Warnings.LOGGED_USER_NOT_FOUND.toString());
    	}
	}


	public void checkPermissionsToCreateUser(RoutingContext rc) {
		System.out.println("Create employee check permissions");
		User user = rc.user();
		if (user != null) {
			JsonObject principal = user.principal();
			String authName = principal.getString("username");
			JsonObject userData = rc.getBodyAsJson();
			String newUserAuthName = userData.getString("authName");
			if (authName.equals(newUserAuthName)) {
				rc.next();
			} else {
				rc.response().setStatusCode(401).end(Warnings.NOT_PERMISSIONS_TO_CREATE_USER.toString());
			}
		} else {
			rc.response().setStatusCode(401).end(Warnings.AUTH_ERROR.toString());
		}
	}

	public void checkPermissionsToAffectUser(RoutingContext rc) {
		System.out.println("Create employee check permissions");
		User user = rc.user();
		if (user != null) {
			JsonObject principal = user.principal();
			String authName = principal.getString("username");
			JsonObject userData = rc.getBodyAsJson();
			String newUserAuthName = userData.getString("authName");
			if (authName.equals(newUserAuthName)) {
				rc.next();
			} else {
				rc.response().setStatusCode(401).end(Warnings.NOT_PERMISSIONS_TO_CREATE_USER.toString());
			}
		} else {
			rc.response().setStatusCode(401).end(Warnings.AUTH_ERROR.toString());
		}
	}
}
