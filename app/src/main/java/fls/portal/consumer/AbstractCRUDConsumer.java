package fls.portal.consumer;

import fls.portal.Neo4j;
import fls.portal.Neo4jMapping;
import fls.portal.Warnings;
import fls.portal.model.Identifiable;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;

/**
 * Created by Pavel on 15.11.2015.
 */
public abstract class AbstractCRUDConsumer<T extends Identifiable> implements CRUDEventConsumer {

    private static final String CREATE_QUERY_TEMPLATE = "CREATE (e: %s {props} ) return id(e)";
    private static final String GET_BY_ID_QUERY_TEMPLATE = "MATCH n	WHERE id(n)={nodeId}  RETURN n";
    private static final String UPDATE_QUERY_TEMPLATE = "MATCH n WHERE id(n)={nodeId} SET n = {props} RETURN n";
    private static final String DELETE_QUERY_TEMPLATE = "MATCH n WHERE id(n)={nodeId}  DELETE n";

    public static final String ID_PARAM_NAME = "nodeId";
    private static final String DATA_PARAM_NAME = "props";
    // это поле будет добавлено в объект, если по объекту будет поиск
    private static final String SEARCH_PARAM_NAME = "searchString";

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCRUDConsumer.class);

    @Override
    public void deploy(EventBus eventBus) {

        String createQuery = String.format(CREATE_QUERY_TEMPLATE,getBaseEvent());

        eventBus.consumer(getCreateEvent(), rc -> {
            LOGGER.debug("Processing event: " + getCreateEvent());
            JsonObject nodeData = (JsonObject) rc.body();
            LOGGER.debug("Storing data:" + nodeData);

            JsonObject message = new JsonObject()
                    .put("query", createQuery)
                    .put("params", getCreateObject(nodeData));
            LOGGER.debug("Sending message:" + message);

            eventBus.send(Neo4j.EVENT_NAME, message, cypherResponse -> {
                if (cypherResponse.succeeded()) {
                    // удачно сохранили в neo4j, надо достать и отправить id
                    JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
                    Long id = neo4jResponseData.getJsonArray("data").getJsonArray(0).getLong(0);
                    JsonObject eventResponse = new JsonObject().put("id", id);
                    rc.reply(eventResponse);
/*
                    nodeData.put("neo4jId", id);
                    eventBus.send("es.addEmployee", nodeData, esResp-> {

                        if (esResp.succeeded()) {
                            System.out.println("ES response: " + esResp.result().body());
                            rc.reply(nodeData);
                        } else {

                            JsonObject errMessage = Warnings.ERROR_CREATE_USER_ES.copy();
                            String originalMessage = Utils.getFailMessage(esResp);
                            message.put("originalMessage", originalMessage);
                            rc.fail(0, message.toString());
                        }
                    });
*/
                } else {
                    // сохранение с ошибкой, отправлям fail
                    rc.fail(0, cypherResponse.cause().getMessage());
                }
            });
        });

        eventBus.consumer(getGetByIdEvent(), rc -> {
            LOGGER.debug("Processing event: " + getGetByIdEvent());
            Long nodeId = (Long) rc.body();
            LOGGER.debug("id:" + nodeId);

            JsonObject message = new JsonObject()
                    .put("query", GET_BY_ID_QUERY_TEMPLATE)
                    .put("params", getIdObject(nodeId));
            LOGGER.debug("Sending message:" + message);

            eventBus.send(Neo4j.EVENT_NAME, message, cypherResponse -> {
                if (cypherResponse.succeeded()) {
                    JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
                    LOGGER.debug(neo4jResponseData);
                    JsonObject nodeData = Neo4jMapping.mapSingleEmployee(neo4jResponseData);
                    rc.reply(nodeData);
                } else {
                    JsonObject response = new JsonObject()
                            .put("message", cypherResponse.cause().getMessage());
                    rc.reply(response);
                }
            });
        });

        eventBus.consumer(getUpdateEvent(), rc -> {
            LOGGER.debug("Processing event: " + getUpdateEvent());
            JsonObject updateMessage = (JsonObject) rc.body();
            Long nodeId = updateMessage.getLong("id");
            JsonObject nodeData = updateMessage.getJsonObject("editData");

            JsonObject message = new JsonObject()
                    .put("query", UPDATE_QUERY_TEMPLATE)
                    .put("params", getUpdateObject(nodeId, nodeData));
            LOGGER.debug("Sending message:" + message);

            eventBus.send(Neo4j.EVENT_NAME, message, cypherResponse -> {
                LOGGER.debug("response");
                JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
                JsonArray rows = neo4jResponseData.getJsonArray("data");
                if (rows.size() == 1) {
                    LOGGER.debug(neo4jResponseData);
                    rc.reply(rows.getJsonArray(0).getJsonObject(0));
                } else if (rows.size() == 0) {
                    LOGGER.debug("При изменении сотрудника, не найден в neo4j с id: " + updateMessage.getInteger("id"));
                    rc.fail(0, "");
                } else {
                    LOGGER.debug("Ошибка в neo4j, найдено больше одного сотрудника с id: " + updateMessage.getInteger("id"));
                    rc.fail(0, "");
                }

            });
        });

        eventBus.consumer(getDeleteEvent(), rc -> {
            LOGGER.debug("Processing event: " + getDeleteEvent());
            Long nodeId = (Long) rc.body();

            JsonObject message = new JsonObject()
                    .put("query", DELETE_QUERY_TEMPLATE)
                    .put("params", getIdObject(nodeId));
            LOGGER.debug("Sending message:" + message);

            eventBus.send(Neo4j.EVENT_NAME, message, cypherResponse -> {
                JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
                LOGGER.debug(neo4jResponseData);
                rc.reply(neo4jResponseData);
            });

        });

    }


    protected abstract Class<T> getModelClass();


    @Override
    public String getBaseEvent() {
        return getModelClass().getSimpleName();
    }

    // если по объекту нужен поиск, то создаём дополнительное поле searchString (индексируемое)
    // как конкантенация полей по которым будет поиск
    private JsonObject getCreateObject(JsonObject nodeData) {
    	JsonObject saveData = nodeData;
    	if (isSearchable()) {
    		String indexString = getIndexString(nodeData);
    		saveData.put(SEARCH_PARAM_NAME, indexString);
    	}
        JsonObject jsonObject = new JsonObject()
                .put(DATA_PARAM_NAME, saveData);
        return jsonObject;
    }

    private JsonObject getIdObject(long nodeId) {
        JsonObject jsonObject = new JsonObject()
                .put(ID_PARAM_NAME, nodeId);
        return jsonObject;
    }

    private JsonObject getUpdateObject(Long nodeId, JsonObject nodeData) {
        JsonObject jsonObject = getIdObject(nodeId)
                .put(DATA_PARAM_NAME, nodeData);
        return jsonObject;
    }


    @Override
    public String getSearchEvent() {
        return getBaseEvent().concat(SEARCH_SUFFIX);
    }

    @Override
    public String getCreateEvent() {
        return getBaseEvent().concat(CREATE_SUFFIX);
    }

    @Override
    public String getGetByIdEvent() {
        return getBaseEvent().concat(GET_BY_ID_SUFFIX);
    }

    @Override
    public String getUpdateEvent() {
        return getBaseEvent().concat(UPDATE_SUFFIX);
    }

    @Override
    public String getDeleteEvent() {
        return getBaseEvent().concat(DELETE_SUFFIX);
    }

    @Override
    // по умолчанию, поиска нет
	public boolean isSearchable() {
		return false;
	}

	@Override
	public String getIndexString(JsonObject data) {
		return null;
	}

}
