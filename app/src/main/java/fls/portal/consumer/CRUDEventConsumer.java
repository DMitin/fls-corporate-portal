package fls.portal.consumer;

import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

/**
 * Created by Pavel on 16.11.2015.
 */
public interface CRUDEventConsumer {
    String SEARCH_SUFFIX = ".search";
    String CREATE_SUFFIX = ".create";
    String GET_BY_ID_SUFFIX = ".getById";
    String UPDATE_SUFFIX = ".update";
    String DELETE_SUFFIX = ".delete";

    String getCreateEvent();

    String getGetByIdEvent();

    String getBaseEvent();

    String getSearchEvent();

    String getUpdateEvent();

    String getDeleteEvent();

    void deploy(EventBus eb);
    
    // Определяет, будет ли осуществляться поиск по данной сущности
    boolean isSearchable();
    /**
     *  Функция, которая будет записана в индекс (поиск по индексу быстрее) для поиска
     *  Возвращаемое значение - комбинация полей в объекте, по которым будет вестись поиск
     * @return
     */
    String getIndexString(JsonObject data);
}

