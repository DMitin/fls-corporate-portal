package fls.portal.consumer.impl;

import fls.portal.consumer.AbstractCRUDConsumer;
import fls.portal.model.Project;

/**
 * Created by Pavel on 16.11.2015.
 */
public class ProjectEventConsumer extends AbstractCRUDConsumer<Project> {
    @Override
    protected Class<Project> getModelClass() {
        return Project.class;
    }
}
