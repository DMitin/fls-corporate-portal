package fls.portal;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
/**
 * Высокоуровневое (бизнес) запросы к neo4j.
 * 1. Подготовка cypher запроса
 * 2. Mapping значения, вернувшегося из neo4j
 *
 * @author DMitin
 *
 */
public class Neo4jApi extends AbstractVerticle {

	@Override
	public void start() {

		EventBus eb = vertx.eventBus();

		/*
		 * Сохранение сотрудника в neo4j
		 */
		String saveEmployeeQuery = "CREATE (e:Employee { props } ) return id(e)";
		JsonObject saveEmployeeTemplate = new JsonObject()
				 .put("query", saveEmployeeQuery)
				 .put("params", new JsonObject());

		//System.out.println("cloned request: " + new JsonObject(saveEmployeeTemplate.toString()));
		//new JsonObject(cypherMessage.encode()).getJsonObject("params").put("props", employeeData);

		eb.consumer("neo4j.api.saveEmployee", saveEmployeeMessage -> {

			JsonObject employeeData = (JsonObject) saveEmployeeMessage.body();
			String authName = employeeData.getString("authName");
			if (authName == null || authName.isEmpty()) {
				saveEmployeeMessage.fail(0, Warnings.CANNOT_SAVE_EMPLOYEE_WITHOUT_AUTHNAME.toString());
			}
			JsonObject req = new JsonObject(saveEmployeeTemplate.toString());
			req.getJsonObject("params").put("props", employeeData);

			eb.send("neo4j.runCypher", req, cypherResponse -> {

				if (cypherResponse.succeeded()) {
					// удачно сохранили в neo4j, надо достать и отправить id
					JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
					Integer id = neo4jResponseData.getJsonArray("data").getJsonArray(0).getInteger(0);
				    JsonObject eventResponse = new JsonObject().put("id", id);
				    saveEmployeeMessage.reply(eventResponse);
				 } else {
					 // сохранение с ошибкой, отправлям fail
					 saveEmployeeMessage.fail(0, cypherResponse.cause().getMessage());
				 }
			});
		});


		/*
		 * Вернуть сотрудника из neo4j по id
		 */
		String query = "MATCH n	WHERE id(n)={employeeId}  RETURN n";
		JsonObject getEmployeeByIdTemplate = new JsonObject()
				.put("query", query)
				.put("params", new JsonObject());

		eb.consumer("neo4j.api.getEmployeeById",  getEmployeeByIdMessage -> {

			String id = (String) getEmployeeByIdMessage.body();
			Integer employeeId = Integer.valueOf(id);
			JsonObject req = getEmployeeByIdTemplate.copy();
			req.getJsonObject("params").put("employeeId", employeeId);
			System.out.println(req);
			eb.send("neo4j.runCypher", req, cypherResponse -> {

				if (cypherResponse.succeeded()) {
					JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
					System.out.println(neo4jResponseData);
					JsonObject employeeData = Neo4jMapping.mapSingleEmployee(neo4jResponseData);
					if (employeeData == null) {
						getEmployeeByIdMessage.reply(Warnings.ERROR_GET_OBJECT_BY_ID.copy()
								.put("errordata", employeeId));
					} else {
						getEmployeeByIdMessage.reply(employeeData);
					}
				} else {
					JsonObject response = new JsonObject()
							.put("message", cypherResponse.cause().getMessage());
					getEmployeeByIdMessage.reply(response);
				}
			});
		});


		/*
		 * Вернуть сотрудника из neo4j по authName
		 * Этот метод не вызывается напрямую из клиенской части, поэтому честно передаём сообщение об ошибке
		 */
		String hetEmployeeByAuthName = "MATCH (e:Employee {authName: {authName}} )  RETURN e";
		JsonObject getEmployeeByAuthNameTemplate = new JsonObject()
				.put("query", hetEmployeeByAuthName)
				.put("params", new JsonObject());

		eb.consumer("neo4j.api.getEmployeeByAuthName",  getEmployeeByAuthNameMessage -> {

			String authName = (String) getEmployeeByAuthNameMessage.body();
			System.out.println("authName: " + authName);
			JsonObject req = getEmployeeByAuthNameTemplate.copy();
			req.getJsonObject("params").put("authName", authName);
			System.out.println(req);
			eb.send("neo4j.runCypher", req, cypherResponse -> {

				if (cypherResponse.succeeded()) {
					JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
					System.out.println(neo4jResponseData);
					JsonObject employeeData = Neo4jMapping.mapSingleEmployee(neo4jResponseData);
					if (employeeData == null) {
						getEmployeeByAuthNameMessage.fail(0, "не важно что");
					} else {
						getEmployeeByAuthNameMessage.reply(employeeData);
					}
				} else {
					getEmployeeByAuthNameMessage.fail(0, "не важно что");
				}
			});
		});


		/*
		 * Изменить данные сотрудника neo4j по id
		 */
		String changeEmployeeQuery =  "MATCH n WHERE id(n)={ employeeId } SET n = { props } RETURN n";
		JsonObject changeEmployeeByIdTemplate = new JsonObject()
				.put("query", changeEmployeeQuery)
				.put("params", new JsonObject());
		eb.consumer("neo4j.api.changeEmployeeById",  changeEmployeeByIdMessage -> {

			JsonObject editEmployeeMessage = (JsonObject) changeEmployeeByIdMessage.body();
			JsonObject req = changeEmployeeByIdTemplate.copy();
			JsonObject newEmployeeData = editEmployeeMessage.getJsonObject("employeeData");
			newEmployeeData.remove("authName"); // нельзя давать поменять authName
			req.getJsonObject("params").put("props", newEmployeeData);
			req.getJsonObject("params").put("employeeId", editEmployeeMessage.getInteger("id"));
			eb.send("neo4j.runCypher", req, cypherResponse -> {
				JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
				JsonArray data = neo4jResponseData.getJsonArray("data");
				if (data.size() == 1) {
					System.out.println(neo4jResponseData);
					changeEmployeeByIdMessage.reply(null);
				} else if (data.size() == 0) {
					System.out.println("При изменении сотрудника, не найден в neo4j с id: " + editEmployeeMessage.getInteger("id"));
					changeEmployeeByIdMessage.fail(0, "");
				} else {
					System.out.println("Ошибка в neo4j, найдено больше одного сотрудника с id: " + editEmployeeMessage.getInteger("id"));
					changeEmployeeByIdMessage.fail(0, "");
				}

			});
		});


		String deleteEmployeeQuery = "MATCH n	WHERE id(n)={employeeId}  DELETE n";
		JsonObject deleteEmployeeByIdTemplate = new JsonObject()
				.put("query", deleteEmployeeQuery)
				.put("params", new JsonObject());

		eb.consumer("neo4j.api.deleteEmployeeById",  deleteEmployeeByIdMessage -> {
			Integer employeeId = (Integer) deleteEmployeeByIdMessage.body();
			JsonObject req = deleteEmployeeByIdTemplate.copy();
			req.getJsonObject("params")
				.put("employeeId", employeeId);

			eb.send("neo4j.runCypher", req, cypherResponse -> {
				JsonObject neo4jResponseData = (JsonObject) cypherResponse.result().body();
				System.out.println(neo4jResponseData);
				deleteEmployeeByIdMessage.reply(neo4jResponseData);
			});

		});



	}

}
