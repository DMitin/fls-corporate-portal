package fls.portal.auth;


import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;


/**
 * Created by DMitin on 20.10.2015.
 */
public class AuthController extends AbstractVerticle {
    @Override
    public void start() {
        System.out.println("AuthController started");
        SimpleLDAPAuthProvider provider = new SimpleLDAPAuthProvider(vertx);

        JsonObject authInfo = new JsonObject();
        authInfo.put("username", "DMitin");
        authInfo.put("password", "qacZ90tr");

        long startTime = System.currentTimeMillis();

        provider.authenticate(authInfo, resp-> {

            long startHandlerTime = System.currentTimeMillis();
            User user = resp.result();
            System.out.println(user);
            if (resp.succeeded()) {
                System.out.println("auth response");
                //System.out.print(user.principal());
                long stopTime = System.currentTimeMillis();
                long elapsedTime = stopTime - startHandlerTime;
                System.out.println("execution handler time: " + elapsedTime);
            } else {
                System.out.println("user not auth ldap");
                long stopTime = System.currentTimeMillis();
                long elapsedTime = stopTime - startHandlerTime;
                System.out.println("execution handler time: " + elapsedTime);
            }
        });


        long stopTime = System.currentTimeMillis();
        long elapsedTime = stopTime - startTime;
        System.out.println("execution time: " + elapsedTime);
    }
}
