package fls.portal.auth;

import java.util.Arrays;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.core.shareddata.LocalMap;
import io.vertx.core.shareddata.SharedData;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;

public class FakeAuthProvider implements AuthProvider {


	private Vertx vertx;
	public FakeAuthProvider(Vertx vertx) {
		this.vertx = vertx;
	}

	@Override
	public void authenticate(JsonObject authInfo, Handler<AsyncResult<User>> resultHandler) {

		String username = authInfo.getString("username");
        if (username == null) {
            resultHandler.handle(Future.failedFuture("authInfo must contain username in \'username\' field"));
            return;
        }
        String password = authInfo.getString("password");
        if (password == null) {
            resultHandler.handle(Future.failedFuture("authInfo must contain password in \'password\' field"));
            return;
        }

        vertx.eventBus().send("neo4j.api.getEmployeeByAuthName", username, resp-> {
        	if (resp.succeeded()) {
        		System.out.println("Получили данные пользователя из neo4j");
            	JsonObject userData = (JsonObject) resp.result().body();
            	System.out.println("auth user: " + userData);
            	SharedData sd = vertx.sharedData();
            	LocalMap<String, JsonObject> map = sd.getLocalMap("authUserData");
            	map.put(username, userData);
        	}
        	resultHandler.handle(Future.succeededFuture(new PortalUser(username, this,  Arrays.asList("employee"))));
        });

	}

}
