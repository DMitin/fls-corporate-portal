package fls.portal.auth;

import io.vertx.core.Handler;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.impl.AuthHandlerImpl;


/**
 * Класс как FormLoginHandlerImpl
 * только данные берутся не из формы, а из ajax
 * @author DMitin
 *
 */
public class AjaxLoginHandler extends AuthHandlerImpl implements Handler<RoutingContext> {


	private static final Logger log = LoggerFactory.getLogger(AuthHandlerImpl.class);
	Handler<RoutingContext> success;
	
	public AjaxLoginHandler(AuthProvider authProvider, Handler<RoutingContext> success) {
		super(authProvider);
		this.success = success;
	}

	@Override
	  public void handle(RoutingContext context) {
	    HttpServerRequest req = context.request();
	    if (req.method() != HttpMethod.POST) {
	      context.fail(405); // Must be a POST
	    } else {	      	      
	    	/*
	      String username = context.request().getParam("username");	     
	      String password = context.request().getParam("password");
	      */
	    	String username = context.getBodyAsJson().getString("username");
	    	String password = context.getBodyAsJson().getString("password");
	      if (username == null || password == null) {
	        log.warn("No username or password provided in form - did you forget to include a BodyHandler?");
	        context.fail(400);
	      } else {	        
	        JsonObject authInfo = new JsonObject().put("username", username).put("password", password);
	        authProvider.authenticate(authInfo, res -> {
	          if (res.succeeded()) {
	            User user = res.result();
	            context.setUser(user);
	            success.handle(context);
	            
	          } else {
	            context.fail(403);  // Failed login
	          }
	        });
	      }
	    }
	  }

}
