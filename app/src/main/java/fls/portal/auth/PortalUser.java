package fls.portal.auth;

import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.AbstractUser;
import io.vertx.ext.auth.AuthProvider;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 20.10.2015
 * Time: 13:19
 */
public class PortalUser extends AbstractUser {

    private AuthProvider authProvider;
    private String username;
    private JsonObject principal;
    private Integer id;
    Set<String> roles;


    public PortalUser(String username, AuthProvider authProvider, List<String> roles) {
        this.username = username;
        this.authProvider = authProvider;
        this.roles = new HashSet<>(roles);
        this.id = id;
    }

    public PortalUser(String username, AuthProvider authProvider, List<String> roles, Integer id) {
        this.username = username;
        this.authProvider = authProvider;
        this.roles = new HashSet<>(roles);
        this.id = id;
    }

    @Override
    protected void doIsPermitted(String permissionOrRole, Handler<AsyncResult<Boolean>> resultHandler) {
        resultHandler.handle(Future.succeededFuture(roles.contains(permissionOrRole)));
    }

    public JsonObject principal() {
        if (this.principal == null) {
            this.principal = (new JsonObject()).put("username", this.username);
        }
        return this.principal;
    }

    @Override
    public void setAuthProvider(AuthProvider authProvider) {
        if (!(authProvider instanceof SimpleLDAPAuthProvider)) {
            throw new IllegalArgumentException("Not a SimpleLDAPAuthProvider");
        }
        this.authProvider = (SimpleLDAPAuthProvider) authProvider;
    }
}
