package fls.portal.auth;

import fls.portal.Warnings;
import io.vertx.ext.auth.AuthProvider;
import io.vertx.ext.auth.User;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.Session;
import io.vertx.ext.web.handler.impl.AuthHandlerImpl;

/**
 * Это класс как RedirectAuthHandlerImpl
 * только, если пользователь не залогинен - не делает redirect - а посылает сообщение об ошибке
 * @author DMitin
 *
 */
public class CheckUserHandler extends AuthHandlerImpl {

	String DEFAULT_RETURN_URL_PARAM = "return_url";

	public CheckUserHandler(AuthProvider authProvider) {
		super (authProvider);
	}

	@Override
	public void handle(RoutingContext context) {
		Session session = context.session();
		if (session != null) {
	    	User user = context.user();
			if (user != null) {
			// Already logged in, just authorise
				authorise(user, context);
			} else {
	        	session.put(DEFAULT_RETURN_URL_PARAM, context.request().path());
	        	context.response().setStatusCode(401).end(Warnings.NOT_PERMISSIONS_TO_CREATE_USER.toString());
	      }
	    } else {
	      context.fail(new NullPointerException("No session - did you forget to include a SessionHandler?"));
	    }

	}

}
