package fls.portal.handler;

import fls.portal.exception.ValidationException;
import fls.portal.model.ErrorResponse;
import fls.portal.model.Pair;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.RoutingContext;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;

import javax.validation.ConstraintViolation;
import java.util.ArrayList;
import java.util.List;

/**
 * User: Pavel Gaev <pavel.gaev@firstlinesoftware.com>
 * Date: 04.11.2015
 * Time: 13:21
 */
public class CommonExceptionHandler implements Handler<RoutingContext> {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonExceptionHandler.class);

    @Override
    public void handle(RoutingContext rc) {
        HttpServerResponse response = rc.response();
        response.putHeader("Content-type", "application/json; charset=utf-8");
        Throwable throwable = rc.failure();
        if (throwable != null) {
            LOGGER.error("handled exception", throwable);
            ErrorResponse errorObject;
            if (throwable instanceof ValidationException) {
                errorObject = processException((ValidationException) throwable, response);
            } else {
                errorObject = processException(throwable, response);
            }

            response.setStatusCode(errorObject.getCode());
            response.end(Json.encodePrettily(errorObject));
        } else {
            rc.next();
        }

    }

    private ErrorResponse processException(ValidationException exception, HttpServerResponse response) {
        String message = StringUtils.isNotBlank(exception.getMessage()) ? exception.getMessage() : "Ошибка валидации данных";
        List<Pair> errors = new ArrayList<>(exception.getViolations().size());
        for (ConstraintViolation violation : exception.getViolations()) {
            errors.add(new Pair(violation.getPropertyPath().toString(), violation.getMessage()));
        }
        return new ErrorResponse(HttpStatus.SC_BAD_REQUEST, exception.getClass().getSimpleName(), message, errors);
    }

    private ErrorResponse processException(Throwable throwable, HttpServerResponse response) {
        String message = "Внутренняя ошибка сервера";
        if (StringUtils.isNotBlank(throwable.getMessage())) {
            message += ": " + throwable.getMessage();
        }
        return new ErrorResponse(HttpStatus.SC_INTERNAL_SERVER_ERROR, throwable.getClass().getSimpleName(), message);
    }
}
