# Данный проект посвящен корпоративному порталу FLS

## Backend stack:
* Vert.x
* Neo4j
## Frontend stack:
* ReactJS
* RxJS
* FabricJS
* ... to be continued

## Как быстро присоединиться к разработке:

* [Frontend](https://bitbucket.org/DMitin/fls-corporate-portal/wiki/%D0%A0%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0%20Frontend)
* Backend

В проекте используется Vagrant, а это значит, что для того, чтобы непосредственно приступить к разработке потребуется всего несколько простых действий

* Скачать и установить virtualbox https://www.virtualbox.org/wiki/Downloads
* Cкачать и установить vagrant http://www.vagrantup.com/downloads.html
* Склонировать данный git repo 
* в консоли перейти в папку репо и ввести команду `vagrant up`
* ...
* PROFIT, вводим `vagrant ssh` и получаем доступ к настроенной среде, которая использует код, находящийся в папке хостовой машины.
* (для Windows пользователей) настраиваем ssh соединение с помощью putty - как описано здесь https://github.com/Varying-Vagrant-Vagrants/VVV/wiki/Connect-to-Your-Vagrant-Virtual-Machine-with-PuTTY 
(для генерации необходимо использовать ключ в самом проекте .vagrant\machines\default\virtualbox\private_key)
* переходим в папку /vagrant (эта папка синхронизируется с родительской системой) [cd /vagrant]
* выполняем
    * run_es (запуск elasticsearch) - [./run_es]
    * run_neo4j (запуск neo4j)
    * run_build (сборка приложения)
    * set_neo4j_password (для изменения пароля по умолчанию) Выполнять эту команду надо только один раз
    * run_vertx (запуск самого приложения)
* если появилось 
~~~~
Подключено к neo4j
~~~~
то сервер запустился и подключился к neo4j

### Для того чтобы обновить конфигурацию, надо выполнить 
~~~~
vagrant provision --provision-with puppet
~~~~

### Запуск rest запросов
Сейчас rest запросы реалезованы в формате программы insomnia http://insomnia.rest/ и только для сотрудников (Employees).
Файл запросов лежит в папке /vagrant/rest/insomnia (надо сделать импорт этого файла).
Можно переопределить в настройках (Ctrl + E) настройки хоста
[пока не работает команда удаления сотрудника]


## Запуск vertx сервера в режиме разработки (для Eclipse)
* Импортировать как maven проект![2015-10-07 20-21-42 Import Maven Projects.png](https://bitbucket.org/repo/A4py97/images/1791583784-2015-10-07%2020-21-42%20Import%20Maven%20Projects.png)
* Запустить neo4j (не забыть изменить пароль при первом запуске) и elasticsearch в vagrant
* прописать в файле /home/vagrant/neo4j/neo4j-community-2.2.5/conf/neo4j-server.properties
~~~~
org.neo4j.server.webserver.address=0.0.0.0
~~~~
![neo4j-conf.png](https://bitbucket.org/repo/A4py97/images/2175073209-neo4j-conf.png)

* Run configurations
![run-configaration.png](https://bitbucket.org/repo/A4py97/images/447839943-run-configaration.png)
![run-configuration2.png](https://bitbucket.org/repo/A4py97/images/2471670817-run-configuration2.png)

## Текущий статус проекта
Собираемся с мыслями и строим планы.